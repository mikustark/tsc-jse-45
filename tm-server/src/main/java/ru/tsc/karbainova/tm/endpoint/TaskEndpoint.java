package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint {

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void clearTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear();
    }

    @WebMethod
    public void createTaskAllParam(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @WebMethod
    public void addTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") TaskDTO task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(task);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") TaskDTO task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @WebMethod
    public List<TaskDTO> findAllTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    public List<TaskDTO> findAllTaskByUserId(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllTaskByUserId(session.getUserId());
    }

    @WebMethod
    public TaskDTO findByIdTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "id") @NonNull String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIdUserId(session.getUserId(), id);
    }

    @WebMethod
    public void addAllTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "tasks") List<TaskDTO> tasks) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().addAll(tasks);
    }
}
