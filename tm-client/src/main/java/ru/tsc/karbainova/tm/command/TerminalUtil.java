package ru.tsc.karbainova.tm.command;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import ru.tsc.karbainova.tm.exception.AbstractException;
import ru.tsc.karbainova.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {
    Scanner scanner = new Scanner(System.in);

    @NonNull
    public static String nextLine() {
        return scanner.nextLine();
    }

    public static Integer nextNumber() throws AbstractException {
        final String value = scanner.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }
}
